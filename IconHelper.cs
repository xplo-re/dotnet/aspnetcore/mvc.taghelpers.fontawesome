﻿/*
 * xplo.re .NET
 *
 * Copyright (C) 2017, xplo.re IT Services, Michael Maier.
 * All rights reserved.
 */

using System;
using JetBrains.Annotations;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc.TagHelpers;
using Microsoft.AspNetCore.Razor.TagHelpers;


namespace XploRe.AspNetCore.Mvc.TagHelpers.FontAwesome
{

    /// <inheritdoc />
    /// <summary>
    ///     Adds support for <c>fa</c> tags that are being transformed into Font Awesome icons. The icon is defined by
    ///     either the <c>icon</c> attribute for a fixed-width icon, or the <c>icon-flexible</c> attribute for a
    ///     flexible width icon. The size of the icon can be controlled by using the optional <c>size</c> attribute.
    ///     Icon names and sizes are not prefixed with the <c>fa-</c> CSS class name prefix. Additional attributes or
    ///     tag contents are retained.
    /// </summary>
    [HtmlTargetElement("fa", Attributes = IconAttributeName)]
    [HtmlTargetElement("fa", Attributes = IconFlexibleAttributeName)]
    public class IconHelper : SizeHelper
    {

        private const string IconAttributeName = "icon";
        private const string IconFlexibleAttributeName = "icon-flexible";


        #region Attributes

        /// <summary>
        ///     Name of Font Awesome icon. Will render the icon with a fixed width.
        /// </summary>
        [CanBeNull]
        [HtmlAttributeName(IconAttributeName)]
        public string Icon { get; set; }

        /// <summary>
        ///     Name of Font Awesome icon. Will render the icon with flexible width.
        /// </summary>
        [CanBeNull]
        [HtmlAttributeName(IconFlexibleAttributeName)]
        public string Symbol { get; set; }

        #endregion


        /// <inheritdoc />
        public override void Process(TagHelperContext context, TagHelperOutput output)
        {
            if (context == null) {
                throw new ArgumentNullException(nameof(context));
            }

            if (output == null) {
                throw new ArgumentNullException(nameof(output));
            }

            // Set corresponding FontAwesome classes.
            var tag = new TagBuilder(output.TagName);
            tag.AddCssClass("fa");

            if (!string.IsNullOrWhiteSpace(Icon)) {
                tag.AddCssClass("fa-" + Icon);
                tag.AddCssClass("fa-fw");
            }
            else if (!string.IsNullOrWhiteSpace(Symbol)) {
                tag.AddCssClass("fa-" + Symbol);
            }

            if (!output.Attributes?.ContainsName("aria-hidden") == true) {
                // Per default icons are hidden. This can be overwritten by explicitly setting the attribute.
                tag.Attributes?.Add("aria-hidden", "true");
            }

            output.MergeAttributes(tag);

            output.TagName = "i";
            output.TagMode = TagMode.StartTagAndEndTag;

            // Process size attribute.
            base.Process(context, output);
        }

    }

}
