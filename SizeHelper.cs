﻿/*
 * xplo.re .NET
 *
 * Copyright (C) 2017, xplo.re IT Services, Michael Maier.
 * All rights reserved.
 */

using System;
using JetBrains.Annotations;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc.TagHelpers;
using Microsoft.AspNetCore.Razor.TagHelpers;


namespace XploRe.AspNetCore.Mvc.TagHelpers.FontAwesome
{

    /// <inheritdoc />
    /// <summary>
    ///     Base <see cref="T:Microsoft.AspNetCore.Razor.TagHelpers.TagHelper" /> for FontAwesome symbols that support 
    ///     resizing.
    /// </summary>
    public abstract class SizeHelper : TagHelper
    {

        #region Attributes

        /// <summary>
        ///     Name of symbol size attribute.
        /// </summary>
        protected const string SizeAttributeName = "size";

        /// <summary>
        ///     Size of icon. Accepts either <c>large</c> or <c>lg</c> for a 33% increased icon size or a numeric value
        ///     in the range of 2 to 5 for a size increase of the given factor.
        /// </summary>
        [CanBeNull]
        [HtmlAttributeName(SizeAttributeName)]
        public string Size { get; set; }

        #endregion


        /// <inheritdoc />
        public override void Process([NotNull] TagHelperContext context, [NotNull] TagHelperOutput output)
        {
            if (context == null) {
                throw new ArgumentNullException(nameof(context));
            }

            if (output == null) {
                throw new ArgumentNullException(nameof(output));
            }

            // Set corresponding FontAwesome classes.
            var tag = new TagBuilder(output.TagName);

            if (!string.IsNullOrWhiteSpace(Size)) {
                if (Size.Equals("large", StringComparison.OrdinalIgnoreCase) ||
                    Size.Equals("lg", StringComparison.OrdinalIgnoreCase)) {
                    tag.AddCssClass("fa-lg");
                }
                else {
                    tag.AddCssClass($"fa-{Size}x");
                }
            }

            output.MergeAttributes(tag);
        }

    }

}
